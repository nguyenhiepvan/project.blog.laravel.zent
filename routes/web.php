<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('blog.homepage');
Route::get('/nh.admin', 'AdminController@index')->name('blog.admin');


Route::get('/category/{slug}', 'HomeController@getPostByCategory')->name('blog.getPostByCategory');

Route::get('/tag/{slug}', 'HomeController@getPostByTag')->name('blog.getPostByTag');

Route::get('/post/{slug}', 'HomeController@getPostBySlug')->name('blog.blog-single');


Route::post('/leaveComment/{guest}/{author}','MailController@leaveComment');

Route::get('/search/{keyword}','HomeController@search');

Route::get('/about', function ()
{
	# code...
	return view('about');
})->name('blog.about');

Route::get('/about/{id}', 'HomeController@about')->name('blog.about_author');
Route::get('/contact', function ()
{
	# code...
	return view('contact');
})->name('blog.contact');
Route::get('/email',function ()
{
	# code...
	return view('Email.verifyEmail');
});
Route::post('/email/sendFeedback','MailController@sendFeedback')->name('blog.sendFeedback');
Route::post('/email/sendFeedbackForAuthor/{email_author}','MailController@sendFeedbackForAuthor')->name('blog.sendFeedbackForAuthor');

/*----------------------------------admin-----------------------------------*/

Route::prefix('admin')->group(function ()
{
	# code...
	Auth::routes(['register' => true]);
	Auth::routes(['verify' => true]);
	//Route::get('/register','Auth\RegisterController@showRegistrationForm');
	//Route::post('/register','Auth\RegisterController@register')->name('admin.register');
	
	Route::middleware(['auth:web'])->group(function ()
	{
		# code...
		
		Route::get('/home', 'AdminController@index')->name('admin.home');
		Route::get('/tags',function()
		{
			# code...
			return view('admin.tags.admin_tag');
		})->name('admin.tag');

		Route::get('/tag/getlisttags','AdminTagController@getListTags')->name('admin.getlisttags');

		Route::post('/tag/save','AdminTagController@tagNew')->name('admin.saveNewTag');
		Route::post('/tag/update','AdminTagController@tagUpdate')->name('admin.updateTag');
		Route::get('/tag/edit/{id}','AdminTagController@tagEdit')->name('tag.edit');
		Route::get('/tag/delete/{id}','AdminTagController@tagDelete')->name('tag.delete');

		/*----------------------------------tag-----------------------------------*/

		Route::get('/posts',function()
		{
			# code...
			return view('admin.posts.admin_post');
		})->name('admin.post');
		
		Route::get('/posts/getlistposts','AdminPostController@getListPosts')->name('admin.getlistposts');
		Route::get('/posts/{id}','AdminPostController@show')->name('posts.show');
		Route::post('/posts/create','AdminPostController@create')->name('posts.create');
		Route::get('/posts/{id}/edit','AdminPostController@edit')->name('posts.edit');
		Route::post('/posts/store','AdminPostController@store')->name('posts.store');
		Route::put('/posts/{id}','AdminPostController@update')->name('posts.update');
		Route::delete('/posts/{id}','AdminPostController@destroy')->name('posts.destroy');
	});
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
