<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Category;
use App\Tag;
use App\Post;
use App\User;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $categories = Category::get();
        $tags = Tag::get();   
        $post_lastest = Post::orderBy( 'created_at', 'desc')->take(3)->get();
        $post_populars = Post::orderBy( 'view_count', 'desc')->take(6)->get();
        $random_author = User::inRandomOrder()->first();
        View::share('categories',$categories);
        View::share('tags',$tags);
        View::share('post_lastest',$post_lastest);
        View::share('post_populars',$post_populars);
        View::share('random_author',$random_author);
    }
}
