<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Comment;
class Post extends Model
{
	public function category()
	{
    	# code...
		return $this->belongsTo('App\Category');
	}
	public function user()
	{
		# code...
		return $this->belongsTo('App\User');
	}
	public function tags()
	{
		# code...
		return $this->belongsToMany('App\Tag','post_tags');
	}
	public function Comments()
	{
		
		# code...
		return $this->hasMany('App\Comment');
	}
}
