<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
	public function getParentCategory()
	{
    	# code...
		$categories = Category::get()->where('parent_id',$this->id);
		return $categories;
	}
	public function posts()
	{
		# code...
		return $this->hasMany('App\Post');
	}
}
