<?php

namespace App;
use App\Comment;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
	public function getParentComment()
	{
    	# code...
		$comments = Comment::get()->where('parent_id',$this->id);
		return $comments;
	}
	public function post()
	{
		# code...
		return $this->belongsTo('App\Post');
	}
}
