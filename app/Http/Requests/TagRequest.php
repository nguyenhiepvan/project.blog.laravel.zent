<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'id'=>'integer',
        'name'=>'required|min:5|max:255',
        'slug'=>'required|unique:tags|alpha_dash'
        ];
    }
    public function messages()
    {
        # code...
        return[
        'required' => ':attribute không được để trống',
        'min' => ':attribute phải có tối thiểu :min ký tự',
        'max' => ':attribute có tối đa :max ký tự',
        ];
    }
    public function attributes()
    {
        # code...
        return [
        'name' => 'tên thẻ',
        'slug' => 'đường dẫn',
        ];
    }
}
