<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        'id'=>'integer',
        'title'=>'required|min:5|max:255',
        'slug'=>'required|unique:tags|alpha_dash',
        'description'=>'required|min:5|max:500',
        'thumbnail'=>'required|image',
        'content'=>'required|min:5|max:999999',
        'tag'=>'array',
        'category'=>'required|array',
        ];
    }

    public function messages()
    {
        # code...
        return[
        'required' => ':attribute không được để trống',
        'min' => ':attribute phải có tối thiểu :min ký tự',
        'max' => ':attribute có tối đa :max ký tự',
        ];
    }
    public function attributes()
    {
        # code...
        return [
        'title' => 'tiêu đề bài viết',
        'slug' => 'đường dẫn',
        'description' => 'lời dẫn',
        'thumbnail' => 'ảnh thu nhỏ',
        'content' => 'nội dung bài viết',
        'tag' => 'thẻ bài viết',
        'slug' => 'đường dẫn',
        'category' => 'danh mục bài viết',
        ];
    }
}
