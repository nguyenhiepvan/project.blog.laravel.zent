<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TagRequest;
use App\Http\Requests\PostRequest;
use App\Tag;
use Yajra\Datatables\Datatables;

class AdminTagController extends Controller
{
    //
	
	public function getListTags()
	{
		# code...
		$tags = Tag::orderBy('id','desc')->get();
		//dd($tags);
		return Datatables::of($tags)

		->addIndexColumn()
		->addColumn('action',function ($tag)
		{
			# code...
			return
			'
			<button style="width: 30px;height:30px" data-url="/admin/tag/edit/'.$tag->id.'" class="btn btn-show btn-xs btn-warning btn-edit"><i class="fa fa-pencil"></i></button>
			<button style="width: 30px;height:30px" data-url="/admin/tag/delete/'.$tag->id.'"  class="btn btn-show btn-xs btn-danger btn-delete"><i class="fa fa-trash"></i></button>';
		})
		->editColumn('name',function ($tag)
		{
			# code...
			return $tag->name;
		})
		->editColumn('slug',function ($tag)
		{
			# code...
			return $tag->slug;
			//return $post->description;
		})
		->make(true);
	}
	public function tagNew(TagRequest $request)
	{
		# code...
		$tag = Tag::create($request->all());
		return response()->json(['data'=>$tag]);
	}
	public function tagUpdate(TagRequest $request)
	{
		# code...
		$tag = Tag::where('id',$request->id)->update(array('name' => $request->name, 'slug'=> $request->slug));
		return response()->json(['data'=>$tag]);
	}
	public function tagEdit($id)
	{
		# code...
		$tag = Tag::find($id);
    	//dd($id);
		return response()->json(['data'=>$tag]);
	}

	public function tagDelete($id)
	{
      # code...
		$result = Tag::destroy($id);
		return response()->json(['data'=>$result]);
	}
}
