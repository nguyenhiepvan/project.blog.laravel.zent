<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Post;
use App\Category;
use App\Tag;
use App\User;

class HomeController extends Controller
{


public function index()
{
        # code...
    $id_highlights = DB::table('highlights')->get();
    $posts = Post::whereNotIn('id',  DB::table('highlights')->select('post_id'))->orderBy( 'created_at', 'desc')->paginate(6);
        //dd($posts);
    $post_highlights = array();
    
    $author = DB::table('users')
    ->join('posts', 'users.id', '=', 'posts.user_id')
    ->select('users.*')
    ->orderBy( 'view_count', 'desc')
    ->first();

    foreach ($id_highlights as $highlight) {
            # code...
        array_push($post_highlights, Post::find($highlight->post_id));
    }

    return view('homepage',['posts'=>$posts,'post_highlights'=>$post_highlights,'author'=>$author]);
}

public function getPostBySlug($slug)
{
        # code...
    $post = Post::where('slug',$slug)->get()->first();
        //dd($post);
    $post->view_count++;
    $post->save();
        //dd($post->thumbnail);
    $tags = $post->tags;
    $post_related = Category::where('id',$post->category_id)->first()->posts->random(3);
    $comments = $post->comments->where('approved',1);
        //dd($comments);
    return view('blog-single',['post'=>$post,'tags'=>$tags,'post_related'=>$post_related,'comments'=>$comments]);
}

public function getPostByCategory ($slug)
{
        # code...
    $category = Category::query()
    ->where('slug', $slug)
    ->first();

    $posts = Post::query()
    ->where('category_id', $category->id)
    ->paginate(6);
    
        //dd($posts);
    return view('category',['posts'=>$posts,'cate'=>$category]);
}

public function getPostByTag($slug)
{
        # code...
          # code...
    $tag = Tag::query()
    ->where('slug', $slug)
    ->first();

    $posts = $tag->posts()->paginate(6);

        //dd($posts);
    return view('tag',['posts'=>$posts,'tag'=>$tag]);
}
public function search($keyword,Request $request)
{
        # code...
    $posts = DB::table('users')
    ->join('posts', 'users.id', '=', 'posts.user_id')
    ->select('posts.*')
    ->where('users.name','like','%'.$keyword.'%')
    ->orwhere('posts.title','like','%'.$keyword.'%')
    ->orwhere('posts.description','like','%'.$keyword.'%')
    ->get();
    $results = [];
    foreach ($posts as $post) {
            # code...

        $category = Category::where('id',$post->category_id)->first();
        $user = User::where('id',$post->user_id)->first();
        $result = ['post' => $post,'category'=>$category,'user'=>$user];
        array_push($results, $result);
    }

         // Get current page form url e.x. &page=1
    $currentPage = LengthAwarePaginator::resolveCurrentPage();

        // Create a new Laravel collection from the array data
    $itemCollection = collect($results);

        // Define how many items we want to be visible in each page
    $perPage = 6;

        // Slice the collection to get the items to display in current page
    $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();

        // Create our paginator and pass it to the view
    $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);

        // set url path for generted links
    $paginatedItems->setPath($request->url());

        //dd($posts)
    
    return view('result',['results'=>$paginatedItems,'keyword'=>$keyword]);
    
}
public function about($id)
{
        # code...
    $author = User::find($id);
    $posts_Latest = Post::where('user_id',  $id)
    ->orderBy( 'created_at', 'desc')
    ->paginate(6);
    $posts_popuar_by_author = Post::where('user_id',  $id)
    ->orderBy( 'view_count', 'desc')
    ->take(3)
    ->get();
    return view('about_author',['author'=>$author,'posts_Latest'=>$posts_Latest,'posts_popuar_by_author'=>$posts_popuar_by_author]);
}

}
