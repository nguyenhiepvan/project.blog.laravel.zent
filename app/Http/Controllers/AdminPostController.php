<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TagRequest;
use App\Http\Requests\PostRequest;
use App\Tag;
use App\Post;
use App\User;
use Yajra\Datatables\Datatables;

class AdminPostController extends Controller
{
    //
	public function getListPosts()
	{
    # code...
		$posts = Post::orderBy('id','desc')->get();
    //dd($posts);
		return Datatables::of($posts)

		->addIndexColumn()
		->addColumn('action',function ($post)
		{
      # code...
			return
			'<button style="width: 30px; height: 30px" data-url="#"​ class="btn btn-show btn-xs btn-success " ><i class="fa fa-info"></i></button>
			<button style="width: 30px; height: 30px" data-id="'.$post->id.'" data-url="#"​ class="btn btn-edit btn-xs btn-warning " ><i class="fa fa-pencil"></i></button>
			<button style="width: 30px; height: 30px" data-url="#"​ class="btn btn-xs btn-danger btn-delete" ><i class="fa fa-trash"></i></button>
			';
		})
		->editColumn('title',function ($post)
		{
      # code...
			return $post->title;
		})
		->editColumn('thumbnail',function ($post)
		{
      # code...
			return ' <a href="'.$post->thumbnail.'" data-fancybox="images">
			<img src="'.$post->thumbnail.'"style="
			width: 50px;
			height: 50px;
			">
		</a>';
	})
		->editColumn('slug',function ($post)
		{
      # code...
			return $post->slug;
      //return $post->description;
		})
		->editColumn('user',function ($post)
		{
      # code...
			return $post->user->name;
      //return $post->description;
		})
		->editColumn('updated_at',function ($post)
		{
      # code...
			return $post->updated_at;
      //return $post->description;
		})
		->rawColumns(['thumbnail','action'])
		->make(true);
	}

 public function create()
 {
  $tags = Tag::get();
  return response()->json(['tags' => $tags]);
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
    	$path = '';
    	$date = date('YmdHis');
    	if ($request->thumbnail) {
    		$thumbnail = $request->thumbnail->getClientOriginalName();
    		$path = $request->file('thumbnail')->storeAs('/images/posts', $date . '_' . $thumbnail);
    	}

    	$user_id = Auth::id();
    	$post = Post::create([
    		'title' => $request->title,
    		'content' => $request->content,
    		'description' => $request->description,
    		'slug' => $request->slug,
    		'thumbnail' => env('APP_URL').'storage/'.$path,
    		'user_id' => $user_id,
    		'category_id' => $request->category
    		]);

    	if ($request->post_tag) {
    		$tags = explode(",", $request->post_tag);
    		foreach ($tags as $tag) {
    			PostTag::create([
    				'post_id' => $post->id,
    				'tag_id' => $tag
    				]);
    		}
    	}

    	return response()->json(['success' => 'Thêm mới thành công'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	// $post = Post::find($id);
    	// $user = $post->user->name;
    	// $category = Category::select('name')->where('id', $post->category_id)->first();
    	// $post_tags = $post->tags;

    	// return response()->json([
    	// 	'post' => $post, 
    	// 	'user' => $user, 
    	// 	'category' => $category, 
    	// 	'post_tags' => $post_tags
    	// 	],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$post = Post::where('id', $id)->first();
    	$post_tags = PostTag::select('tag_id')->where('post_id', $post->id)->get();
    	$categories = Category::select('id', 'name')->get();
    	$tags = Tag::select('id', 'name')->get();

    	return response()->json([
    		'post' => $post, 
    		'post_tags' => $post_tags, 
    		'categories' => $categories, 
    		'tags' => $tags
    		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
    	$path = '';
    	$date = date('YmdHis');

    	if ($request->edit_thumbnail) {
    		$thumbnail = $request->edit_thumbnail->getClientOriginalName();
    		$path = $request->edit_thumbnail->storeAs('/images/posts', $date . '_' . $thumbnail);
    		$path = env('APP_URL').'storage/'.$path;
    	} else {
    		$path = $request->thumbnail;
    	}

    	$user_id = Auth::id();
    	Post::where('id', $id)->update([
    		'title' => $request->title,
    		'content' => $request->content,
    		'description' => $request->description,
    		'slug' => $request->slug,
    		'thumbnail' => $path,
    		'user_id' => $user_id,
    		'category_id' => $request->category
    		]);

    	if ($request->post_tag) {
    		$tags = explode(",", $request->post_tag);
    		foreach ($tags as $tag) {
    			PostTag::where('post_id', $id)->updateOrCreate([
    				'post_id' => $id,
    				'tag_id' => $tag
    				]);
    		}
    	}

    	return response()->json(['success' => 'Sửa thành công'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	Post::where('id', $id)->forceDelete();
    	PostTag::where('post_id', $id)->delete();
    	return response()->json(['success' => 'Xóa thành công'], 200);
    }

    public function removePostTag(Request $request, $tag_id)
    {
    	PostTag::where('post_id', $request->post_id)->where('tag_id', $tag_id)->delete();
    }

  }
