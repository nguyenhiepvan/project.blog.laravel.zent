<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TagRequest;
use App\Http\Requests\PostRequest;
use App\Tag;
use App\Post;
use Yajra\Datatables\Datatables;

class AdminController extends Controller
{
    //
      /**
     * Create a new controller instance.
     *
     * @return void
     */
      public function __construct()
      {
        $this->middleware(['auth','verified']);
      	//$this->middleware('auth');
      }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

    	return view('admin.dashboard');
    }

  }
