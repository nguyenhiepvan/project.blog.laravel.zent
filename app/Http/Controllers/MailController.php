<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Http\Requests\FeedbackRequest;
use App\Http\Requests\CommentRequest;
use Illuminate\Support\Facades\DB;
use App\Post;
use App\User;
use App\Comment;

class MailController extends Controller
{
    //
	public function sendFeedback(FeedbackRequest $request)
	{
    	# code...
		$feedback = $request->all();
		$to_name = 'Admin';
		$to_email = 'nguyenhiepvan.bka@gmail.com';
		$content = $feedback['message'];

		try {
			Mail::send('Email.feedback', ['content' => $content,'email'=>$feedback['email'],'name'=>$feedback['name']], function($message) use ($to_name, $to_email)
			{
				$message->to($to_email, $to_name);
				$message->subject('Sir, You may have new messages');
				$message->from('noreply.nhblog@gmail.com', 'No_Reply@NHBlog');
			});
			return response()->json(['success' => 'bọn tao nhận được tin nhắn rồi nhé,có gì báo lại sau,<br>nhớ check mail nhaaaaaa!'], 200);
			
		} catch (Exception $ex) {
			return response()->json(['fail' => 'ơ đệt không gửi được'], 200);
		}
	}
	public function sendFeedbackForAuthor(FeedbackRequest $request,$email_author)
	{
    	# code...
		$feedback = $request->all();
		$to_name = 'Admin';
		$to_email = $email_author;
		$content = $feedback['message'];

		try {
			Mail::send('Email.feedback', ['content' => $content,'email'=>$feedback['email'],'name'=>$feedback['name']], function($message) use ($to_name, $to_email)
			{
				$message->to($to_email, $to_name);
				$message->subject('Ê mày có tin nhắn nè ');
				$message->from('noreply.nhblog@gmail.com', 'No_Reply@NHBlog');
			});
			return response()->json(['success' => 'tao nhận được tin nhắn rồi nhé,có gì báo lại sau,<br>nhớ check mail nhaaaaaa!'], 200);
			
		} catch (Exception $ex) {
			return response()->json(['fail' => 'ơ đệt không gửi được'], 200);
		}
	}

	public function leaveComment(CommentRequest $request,$guest,$author)
	{
		# code...
		$cmt = $request->all();
		DB::table('Comments')->insert($cmt);
		$post = Post::find($cmt['post_id']);
		$guest = Comment::find($guest);
		$author = User::find($author);

		if(count($guest)>0){
			$message_to_guest = '<p>Bình luận của bạn tại bài viết: <a href="http://project.blog.laravel.zent/post/'.$post->slug.'">'.$post->title.'</a> đã được ai đó trả lời.</p>
			<p>Chúng tôi đang trong quá trình kiếm duyệt.</p>
			<p>chúng tôi sẽ thông báo tới bạn sau khi quá trình kiểm duyệt hoàn tất</p>';

			$to_name = $guest->name;
			$to_email = $guest->email;
			try {
				Mail::send('Email.notification_comment', ['content' => $message_to_guest], function($message) use ($to_name, $to_email)
				{
					$message->to($to_email, $to_name);
					$message->subject('Bình luận vài viết tại NH blog');
					$message->from('noreply.nhblog@gmail.com', 'No_Reply@NHBlog');
				});			
			} catch (Exception $ex) {
			}
		}


		$message_to_author ='<p>Bài viết <a href="http://project.blog.laravel.zent/post/'.$post->slug.'">'.$post->title.'</a> của bạn vừa có một bình luận mới</p>
		<p>Bạn hãy tới trang quản trị để hoàn tất quá trình kiểm duyệt nhé!</p>';

		$to_name = $author->name;
		$to_email = $author->email;
		try {
			Mail::send('Email.notification_comment', ['content' => $message_to_author], function($message) use ($to_name, $to_email)
			{
				$message->to($to_email, $to_name);
				$message->subject('Kiểm duyệt bình luận tại NH blog');
				$message->from('noreply.nhblog@gmail.com', 'No_Reply@NHBlog');
			});
			return response()->json(['success' => 'Bình luận đang trong quá trình kiểm duyệt, chúng tôi sẽ thông báo tới bạn khi hoàn tất'], 200);
			
		} catch (Exception $ex) {
			return response()->json(['fail' => 'ơ đệt không gửi được'], 200);
		}
	}

}
