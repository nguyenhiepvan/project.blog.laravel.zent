$('#feedback-for-admin').click(function(){
  var response = grecaptcha.getResponse();
  if(response.length == 0)
   $('.error-captcha').html('Lỗi captcha nhaaaa!');
 else
 {
  $('#feedback-for-admin').toggleClass('running');
  $('#content_btn').html('Chờ chút, đang gửi ạ');
  $('.error').html('');
  $.ajax({
   type:'post',
   data:{
    'name': $('#name').val(),
    'email': $('#email').val(),
    'message': $('#message').val()
  },
  dataType:'JSON',
  url: '/email/sendFeedback',
  success: function(res) {
    if(res.success!='')
    {
     Swal.fire({
      position: 'middle',
      type: 'success',
      title: res.success,
      showConfirmButton: false,
      timer: 4000
    });
     $('#feedback-form')[0].reset();
     grecaptcha.reset();
     $('#feedback-for-admin').toggleClass('running');
     $('#content_btn').html('Để lại lời nhắn');
   }
   else
   {
    $('#feedback-for-admin').toggleClass('running');
    $('#content_btn').html('Để lại lời nhắn');
    Swal.fire({
      type: 'error',
      title: 'Oops...',
      text: res.fail
    })
  }
},
error: function(error){
 $('#feedback-for-admin').toggleClass('running');
 $('#content_btn').html('Để lại lời nhắn');
 $.each(error.responseJSON.errors,function(key,value) {

  $('.'+key+'-error').html(value);
})
}
});
}
$('#message-for-author').click(function(){
  var response = grecaptcha.getResponse();
  var email_author =  $('#message-form').attr('data-url');
  if(response.length == 0)
   $('.error-captcha').html('Lỗi captcha nhaaaa!');
 else
 {
  $('#message-for-author').toggleClass('running');
  $('#content_btn').html('Chờ chút, đang gửi ạ');
  $('.error').html('');
  $.ajax({
   type:'post',
   data:{
    'name': $('#name').val(),
    'email': $('#email').val(),
    'message': $('#message').val()
  },
  dataType:'JSON',
  url: '/email/sendFeedbackForAuthor/'+email_author,
  success: function(res) {
    if(res.success!='')
    {
     Swal.fire({
      position: 'middle',
      type: 'success',
      title: res.success,
      showConfirmButton: false,
      timer: 4000
    });
     $('#message-form')[0].reset();
     grecaptcha.reset();
     $('#message-for-author').toggleClass('running');
     $('#content_btn').html('Để lại lời nhắn');
   }
   else
   {
    $('#message-for-author').toggleClass('running');
    $('#content_btn').html('Để lại lời nhắn');
    Swal.fire({
      type: 'error',
      title: 'Oops...',
      text: res.fail
    })
  }
},
error: function(error){
 $('#message-for-author').toggleClass('running');
 $('#content_btn').html('Để lại lời nhắn');
 $.each(error.responseJSON.errors,function(key,value) {

  $('.'+key+'-error').html(value);
})
}
});
}

});
});

