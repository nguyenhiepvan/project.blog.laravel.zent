$(document).ready(function () {
	// body...

	$(".reply").click(function () {
	// body...
	$('#leave_comment').attr('data-parent',$(this).attr('data-parent'));
});
});

$('#comment-for-post').click(function(){
	var response = grecaptcha.getResponse();
	var parent_id = $('#leave_comment').attr('data-parent');
	var data_guest = $('#leave_comment').attr('data-guest');
	var data_author = $('#leave_comment').attr('data-author');
	var post_id = $('#leave_comment').attr('data-post');

	if(response.length == 0)
		$('.error-captcha').html('Lỗi captcha nhaaaa!');
	else
	{
		$('#comment-for-post').toggleClass('running');
		$('#content_btn').html('Chờ chút, đang đăng ạ');
		$('.error').html('');

		$.ajax({
			type:'post',
			data:{
				'name': $('#name').val(),
				'email': $('#email').val(),
				'message': $('#message').val(),
				'post_id': post_id,
				'parent_id': parent_id,
			},
			dataType:'JSON',
			url: '/leaveComment/'+data_guest+'/'+data_author,
			success: function(res) {
				if(res.success!='')
				{
					Swal.fire({
						position: 'middle',
						type: 'success',
						title: res.success,
						showConfirmButton: false,
						timer: 4000
					});
					$('#comment-form')[0].reset();
					grecaptcha.reset();
					$('#comment-for-post').toggleClass('running');
					$('#content_btn').html('Để lại lời nhắn');
					$('#leave_comment').attr('data-parent','');
				}
				else
				{
					$('#comment-for-post').toggleClass('running');
					$('#content_btn').html('Để lại lời nhắn');
					Swal.fire({
						type: 'error',
						title: 'Oops...',
						text: res.fail
					})
				}
			},
			error: function(error){
				$('#comment-for-post').toggleClass('running');
				$('#content_btn').html('Để lại lời nhắn');
				$.each(error.responseJSON.errors,function(key,value) {

					$('.'+key+'-error').html(value);
				})
			}
		});
	}
});