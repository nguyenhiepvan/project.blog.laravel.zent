$(document).ready(function(){

	var table = $('#tags-table').DataTable({
		processing: true,
		serverSide: true,
		ajax: 'tag/getlisttags',
		columns: [
		{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
		{ data: 'name', name: 'name'},
		{ data: 'slug', name: 'slug'},
		{ data: 'action', name: 'action'}
		],
		columnDefs: [ {
			targets: [3], /* column index */
			orderable: false, /* true or false */
		}]
	});

	$('#add_tag_modal').modal('hide');
	$('#edit_tag_modal').modal('hide');

	$('#add_button').click(function() {
			// body...
			$('#add_tag_modal').modal('show');
		});

})

$('#tags-table').on('click', '.btn-edit', function () {
		// body...
		var url = $(this).attr('data-url');
		//console.log(url);
		$.ajax({
			url: url,
			type: 'get',
			success: function(res) {
				$('#name_edit').val(res.data.name);
				$('#slug_edit').val(res.data.slug);
				$('#id').val(res.data.id);
				$('#edit_tag_modal').modal('show');
			}
		});
		
	})

$(document).on('submit', '#add_tag_form', function (e) {
	e.preventDefault();
	var url = $(this).attr('data-url');
	$.ajax({
		type:'post',
		data:{
			name: $('#name').val(),
			slug: $('#slug').val(),
		},
		url: url,
		success: function() {
					// body...
					$('#add_tag_modal').modal('hide');
					toastr.success('Thông báo', 'Thêm mới thành công', {timeOut: 1000});
					$('#tags-table').DataTable().ajax.reload();
				},
				error: function(error){
					$.each(error.responseJSON.errors,function(key,value) {
						// body...
						$('.'+key+'-error').html(value);
					})
				}
			});
})

$(document).on('submit', '#edit_tag_form', function (e) {
	e.preventDefault();
	var url = $(this).attr('data-url');
	$.ajax({
		type:'POST',
		data:{
			id: $('#id').val(),
			name: $('#name_edit').val(),
			slug: $('#slug_edit').val(),
		},
		url: url,
		success: function() {
					// body...
					$('#edit_tag_modal').modal('hide');
					toastr.success('Thông báo', 'Cập nhập thành công', {timeOut: 1000});
					$('#tags-table').DataTable().ajax.reload();
				},
				error: function(error){
					$.each(error.responseJSON.errors,function(key,value) {
						// body...
						$('.'+key+'-error').html(value);
					})
				}
			});
})

$('#tags-table').on('click', '.btn-delete', function () {
		// body...
		if(confirm('Are you sure?'))
		{
			var url = $(this).attr('data-url');
		//console.log(url);
		$.ajax({
			url: url,
			type: 'get',
			success: function() {
				toastr.success('Thông báo', 'Xóa thành công', {timeOut: 1000});
				$('#tags-table').DataTable().ajax.reload();
			}
		});
	}

})