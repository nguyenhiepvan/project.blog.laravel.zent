<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        for ($i=0; $i < 50; $i++) { 
        	# code...
        	DB::table('posts')->insert([
                'title' => $faker->text($maxNbChars=100),
                'thumbnail' => $faker->imageUrl($width = 100,$height=100),
                'description'=>$faker->text($maxNbChars=10000),
                'slug'=>$faker->slug(),
                'user_id'=>1,
                'category_id'=>1
                ]);
        }
    }
}
