<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    	$faker = Faker\Factory::create();
    	for ($i=0; $i < 50; $i++) { 
        	# code...
    		DB::table('categories')->insert([
    			'name'=>$faker->text($maxNbChars=100),
    			'thumbnail'=>$faker->imageUrl($width=100,$height=100),
    			'slug'=>$faker->slug()
    			]);
    	}
    }
}
