<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
	return [
        //
	'content'=>$faker->text($maxNbChars=1500),
	'name' => $faker->name,
	'email' => $faker->unique()->safeEmail,
	'parent_id'=>rand(0,20),
	'post_id'=>rand(201,300)
	];
});
