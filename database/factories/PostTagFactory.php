<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\PostTag;
use Faker\Generator as Faker;

$factory->define(PostTag::class, function (Faker $faker) {
	return [
        //
	'post_id'=>rand(201,300),
	'tag_id'=>rand(1,10)
	];
});
