<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
	return [
        //
	'name'=>$faker->text($maxNbChars=10),
	'thumbnail'=>$faker->imageUrl($width=100,$height=100),
	'slug'=>$faker->slug(),
	'parent_id'=>rand(1,10),
	'approved'=>rand(0,1)
	];
});
