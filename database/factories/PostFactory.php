<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
	return [
        //
	'title' => $faker->text($maxNbChars=30),
	'thumbnail' => $faker->imageUrl($width = 500, $height = 500),
	'description'=>$faker->text($maxNbChars=150),
	'content'=>$faker->text($maxNbChars=1500),
	'slug'=>$faker->slug(),
	'user_id'=>rand(1,10),
	'view_count'=>rand(1,1000),
	'category_id'=>rand(7,18),
	];
});
