@extends('layouts.master')
@section('content')
<section class="site-section pt-5">
  <div class="container">

    <div class="row blog-entries">
      <div class="col-md-12 col-lg-8 main-content">

        <div class="row">
          <div class="col-md-12" style="text-align: justify;">
            <h2 class="mb-4">Xin chào! Tôi là {{$author->name}}</h2>
            <p class="mb-5"><img src="{{$author->avatar}}" alt="Image placeholder" class="img-fluid"></p>
            {!!html_entity_decode($author->about)!!}
          </div>
        </div>

        <div class="row mb-5 mt-5">
         <div class="col-md-12 mb-5">
          <h2>Bài đăng mới nhất của tôi</h2>
        </div>
        <div class="col-md-12">
          @foreach($posts_Latest as $post)
          <div class="post-entry-horzontal">
            <a href="{{route('blog.blog-single',$post->slug)}}">
              <div class="image" style="background-image: url({{$post->thumbnail}});"></div>
              <span class="text">
                <div class="post-meta">
                  <span class="author mr-2"><img src="{{$author->avatar}}" alt="{{$author->name}}"> {{$author->name}}</span>&bullet;
                  <span class="mr-2">{{date_format($post->created_at, 'l jS F Y')}}</span> &bullet;
                  <span class="ml-2"><span class="fa fa-comments"></span> {{$post->comments->count()}}</span>
                </div>
                <h2>{{$post->title}}</h2>
              </span>
            </a>
          </div>
          @endforeach
          <!-- END post -->

        </div>
      </div>

      <div class="row">
        <div class="col-md-12 text-center">
          <nav aria-label="Page navigation" class="text-center">
            {{ $posts_Latest->onEachSide(6)->links() }}
          </nav>
        </div>
      </div>



    </div>

    <!-- END main-content -->

    <div class="col-md-12 col-lg-4 sidebar">
      <div class="sidebar-box search-form-wrap">
        <form  action="javascript:;" class="search-form">
          <div class="form-group">
            <span class="icon fa fa-search"></span>
            <input type="text" class="form-control" id="search-on-page" placeholder="Nhập từ khóa và nhần enter!">
          </div>
        </form>
      </div>
      <!-- END sidebar-box -->
      <div class="sidebar-box">
        <h3 class="heading">Tác giả khác</h3>
        <div class="bio text-center">
          <img src="{{$random_author->avatar}}" alt="Image Placeholder" class="img-fluid">
          <div class="bio-body">
            <h2>{{$random_author->name}}</h2>
            <p>{{$random_author->quote}}</p>
            <p><a href="{{route('blog.about_author',$random_author->id)}}" class="btn btn-primary btn-sm rounded">xem thêm</a></p>
            <p class="social">
              <a href="#" class="p-2"><span class="fa fa-facebook"></span></a>
              <a href="#" class="p-2"><span class="fa fa-youtube-play"></span></a>
            </p>
          </div>
        </div>
      </div>
      <!-- END sidebar-box -->  
      <div class="sidebar-box">
        <h3 class="heading">Bài đăng phổ biến của tôi</h3>
        <div class="post-entry-sidebar">
          <ul>
            @foreach($posts_popuar_by_author as $post)
            <li>
              <a href="/post/{{$post->slug}}">
                <img src="{{ $post->thumbnail}}" alt="Image placeholder" class="mr-4">
                <div class="text">
                  <h4>{{$post->title}}</h4>
                  <div class="post-meta">
                    <span class="mr-2">{{date_format($post->created_at, 'l jS F Y')}}</span>
                  </div>
                </div>
              </a>
            </li>
            @endforeach
          </ul>
        </div>
      </div>

      <div class="sidebar-box">        
       <h3 class="heading">Để lại lời nhắn cho tôi</h3>
       <form action="javascript:;" data-url="{{$author->email}}"  method="post" id="message-form">
         <input type="hidden" name="_token" value="{{ csrf_token() }}">
         <div class="row">
          <div class="col-md-12 form-group">
            <label for="name">Tên<span class="error">(*)</span>:</label>
            <input type="text" id="name" class="form-control ">
            <span class="name-error error" role="alert">
            </span>
          </div>
          <div class="col-md-12 form-group">
            <label for="email">Email<span class="error">(*)</span>:</label>
            <input type="email" id="email" class="form-control ">
            <span class="email-error error" role="alert">
            </span>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 form-group">
            <label for="message">Lời nhắn<span class="error">(*)</span>:</label>
            <textarea name="message" id="message" class="form-control " cols="30" rows="8"></textarea>
            <span class="message-error error" role="alert">
            </span>
          </div>
        </div><br>
        <div class="form-group">
          <!-- Google reCaptcha -->
          <div class="g-recaptcha" id="feedback-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY')  }}"></div>
          <!-- End Google reCaptcha -->
          <span class="error-captcha error"></span>
        </div>
        <div class="row">
          <div class="col-md-6 form-group">
            <div class="btn btn-primary ld-ext-right" id="message-for-author">
              <span id="content_btn">Để lại lời nhắn</span>
              <div class="ld ld-ball ld-bounce"></div>
            </div>
          </div>
        </div>
      </form>
    </div>

  </div>
</div>
</section>

@endsection
@section('script')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="{{ asset('blog_assets/js/sendfeedback.js')}}"></script>
@endsection