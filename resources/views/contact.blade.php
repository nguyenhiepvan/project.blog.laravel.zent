@extends('layouts.master')
@section('content')
<section class="site-section">
  <div class="container">
    <div class="row mb-4">
      <div class="col-md-6">
        <h1>Liên hệ với tôi</h1>
      </div>
    </div>
    <div class="row blog-entries">
     <div class="col-md-12 col-lg-8 main-content">

       <form action="javascript:;"  method="post" id="feedback-form">
         <input type="hidden" name="_token" value="{{ csrf_token() }}">
         <div class="row">
          <div class="col-md-12 form-group">
            <label for="name">Tên<span class="error">(*)</span>:</label>
            <input type="text" id="name" class="form-control ">
            <span class="name-error error" role="alert">
            </span>
          </div>
          <div class="col-md-12 form-group">
            <label for="email">Email<span class="error">(*)</span>:</label>
            <input type="email" id="email" class="form-control ">
            <span class="email-error error" role="alert">
            </span>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 form-group">
            <label for="message">Lời nhắn<span class="error">(*)</span>:</label>
            <textarea name="message" id="message" class="form-control " cols="30" rows="8"></textarea>
            <span class="message-error error" role="alert">
            </span>
          </div>
        </div>
        <div class="form-group">
          <!-- Google reCaptcha -->
          <div class="g-recaptcha" id="feedback-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY')  }}"></div>
          <!-- End Google reCaptcha -->
          <span class="error-captcha error"></span>
        </div>
        <div class="row">
          <div class="col-md-6 form-group">
            <div class="btn btn-primary ld-ext-right" id="feedback-for-admin">
              <span id="content_btn">Để lại lời nhắn</span>
              <div class="ld ld-ball ld-bounce"></div>
            </div>
          </div>
        </div>
      </form>


    </div>

    <!-- END main-content -->

    <div class="col-md-12 col-lg-4 sidebar">
      <div class="sidebar-box search-form-wrap">
        <form  class="search-form">
          <div class="form-group">
            <span class="icon fa fa-search"></span>
            <input type="text" class="form-control " id="search-on-page" placeholder="Nhập từ khóa và nhần enter!">
          </div>
        </form>
      </div>
      <!-- END sidebar-box -->
      <div class="sidebar-box">
        <h3 class="heading">Tác giả khác</h3>
        <div class="bio text-center">
          <img src="{{$random_author->avatar}}" alt="Image Placeholder" class="img-fluid">
          <div class="bio-body">
            <h2>{{$random_author->name}}</h2>
            <p>{{$random_author->quote}}</p>
            <p><a href="{{route('blog.about_author',$random_author->id)}}" class="btn btn-primary btn-sm rounded">xem thêm</a></p>
            <p class="social">
              <a href="#" class="p-2"><span class="fa fa-facebook"></span></a>
              <a href="#" class="p-2"><span class="fa fa-youtube-play"></span></a>
            </p>
          </div>
        </div>
      </div>
      <!-- END sidebar-box -->  
    </div>
  </div>
</section>
@endsection

@section('script')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="{{ asset('blog_assets/js/sendfeedback.js')}}"></script>
@endsection