@extends('admin.layouts.master')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Quản lý
		<small>Bài viết</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">quản lý bài viết</li>
	</ol>
</section>
<section class="content">
	<button type="button" class="btn btn-success" id="add_button"><i class="fa fa-plus"></i></button>

	<table class="table table-hover table-bordered table-striped" id="posts-table">

		<thead>
			<tr>
				<th>STT</th>
				<th>Tiêu đề</th>
				<th>Ảnh thu nhỏ</th>
				<th>Đường dẫn</th>
				<th>Tác giả</th>
				<th>Cập nhập lần cuối</th>
				<th style="
				width: 11%;
				">#</th>
			</tr>
		</thead>
	</table>
</section>

@include('admin.posts.add')
@include('admin.posts.edit')
@include('admin.posts.show')
@endsection
@section('ajax')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script>
	CKEDITOR.replace( 'editor' );
</script>
<script src="{{asset('admin_assets/js/custom_post.js')}}"></script>

<script type="text/javascript" src="{{ asset('admin_assets/jquery.magicsearch.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
@endsection