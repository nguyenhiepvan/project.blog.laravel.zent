<!-- Modal Edit-->
<div class="modal fade" id="edit_tag_modal" tabindex="-1" role="dialog" aria-labelledby="edit_tag_modal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" >Update Tag</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form  method="post" role = "form" id="edit_tag_form" data-url="{{route('admin.updateTag')}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="id" id="id">
					<div class="form-group">
						<label for="name">Name (<span style="color: red;">*</span>)</label>
						<input type="text" name="name" onkeyup="ChangeToSlug('name_edit', 'slug_edit');" class="form-control" id="name_edit">
						<span class="name-error error">
						</span>
					</div>
					<div class="form-group">
						<label for="slug">Slug (<span style="color: red;">*</span>)</label>
						<input type="text"  name="slug" class="form-control" id="slug_edit">
						<span class="slug-error error">
						</span>
					</div>

					<div class="form-group">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</form>

			</div>
		</div>
	</div>