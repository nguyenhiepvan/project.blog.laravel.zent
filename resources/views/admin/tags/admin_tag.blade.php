@extends('admin.layouts.master')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Quản lý
		<small>thẻ</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">quản lý thẻ</li>
	</ol>
</section>
<section class="content">
	<button type="button" class="btn btn-success" id="add_button"><i class="fa fa-plus"></i></button>

	<table class="table table-hover table-bordered table-striped" id="tags-table">

		<thead>
			<tr>
				<th>STT</th>
				<th>Tên thẻ</th>
				<th>slug</th>
				<th>#</th>
			</tr>
		</thead>
	</table>
</div>
</div>
</section>

@include('admin.tags.add')
@include('admin.tags.edit')
@endsection
@section('ajax')
<script src="{{asset('admin_assets/js/custom_tag.js')}}"></script>
@endsection