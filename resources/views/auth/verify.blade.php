@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Xác minh tài khoản của bạn') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Một email xác minh vừa được gửi tới email của bạn.') }}
                        </div>
                    @endif

                    {{ __('Để trở thành thành viên mới, hãy vào đường liên kết trong email xác minh.') }}
                    {{ __('Trong trường hợp bạn chưa nhận được mail') }}, <a href="{{ route('verification.resend') }}">{{ __('click vào đây để nhận lại email xác nhận') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
