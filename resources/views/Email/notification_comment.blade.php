<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<style type="text/css">
		body{

			background-color: #fecfef;

		}
		.center {
			display: flex;
			margin-left: auto;
			margin-right: auto;
			width: 50%;
		}
		#website h1{
			position: relative;
			left: 50%;
		}
		#logo img{
			width: 60%;
		}
		.responsive {
			width: 100%;
			height: auto;
		}
		.content{

			margin-left: 10%;
			margin-right: 10%;

		}
		.content p{
			text-align: justify;
		}
		.sign{
			text-align: right !important;
			font-family: Helvetica, Arial, sans-serif;
			font-size:18px;
			font-style: bold;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="logo center">
			<div id="website" class="responsive">
				<h1 class="site-logo"><p>NH Blog</p></h1>
			</div>
			<div id="logo" class="responsive">
				<a href="http://project.blog.laravel.zent/"><img src="http://drive.google.com/uc?export=view&id=1EuRBTPdVSa_ww6hnklizINgXuJp_EVP2"></a>
			</div>	
		</div>
		<div class="content">
			@php echo $content @endphp
			<p class="sign"><strong>Thanks and kind regards</strong></p>
		</div>
	</div>
</body>
</html>