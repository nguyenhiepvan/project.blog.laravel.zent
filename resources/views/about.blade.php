@extends('layouts.master')
@section('content')
<section class="site-section pt-5">
  <div class="container">

    <div class="row blog-entries">
      <div class="col-md-12 col-lg-8 main-content">

        <div class="row">
          <div class="col-md-12">
            <h2 class="mb-4">Xin chào, tôi là Hiệp</h2>
            <p class="mb-5"><img src="{{ asset('blog_assets/images/img_6.jpg')}}" alt="Image placeholder" class="img-fluid"></p>
            <p>Mình sinh ra tại việt nam.lớn lên trong một gia đình cần cù, giàu lòng yêu nước, sớm giác ngộ lý tưởng cách mạng.</p>
            <p>Theo kể lại thì vừa sinh ra,mình đã kỳ lạ khác thường, không nói không cười, không đi đứng mà chỉ bắt người khác bế ẵm. Ngày mình sinh, trời đất xảy ra nhiều sự lạ, giữa ban trưa mà trời nắng chang chang, đến tối thì trăng lại sáng vằng vặc. Sau một thời gian thì Liên Xô sụp đổ, nhà nước đổi tiền, Mỹ bỏ cấm vận, thực là dấu hiệu của kỳ tài xuất thế.</p>
            <p>Dân trong làng nói hồi đó, có một đạo sĩ câm đi qua làng, thấy mình chơi trước cửa, ngó thấy nhà mình, phía sau nhà có đường đi, trước nhà có sầu riêng, bên trái nhà có cây bơ , bên phải nhà có giếng nước xi măng, mới phán rằng: (Nhà này tọa ngay long mạch, đầu gối sơn, chân đạp thủy, bên tả có thanh long, bên hữu có bạch hổ, con nhà này chỉ mười tám năm sau, chắc chắn sẽ thành người lớn ) Người này mới sinh đã có quý tướng, sau này tất học hành đỗ đạt. Quả nhiên, sau này mình đi học, từ mẫu giáo đã có phiếu bé ngoan, lớn lên không môn nào không thi lại dưới 2 lần, nhưng đấy là chuyện về sau, ở đây không nhắc tới. Mình từ lúc 7 tuổi đã tự biết viết tên. 9 tuổi biết đánh nhau, sang tuổi 12, mọi người trong thiên hạ không ai là không biết tiếng; lại nổi tiếng thông minh đĩnh ngộ, thiên hạ có gì không biết, kéo nhau đến hỏi, mình đều trả lời được cả, mà tuyệt nhiên không đúng câu nào.</p>
            <p>Năm ấy trong làng có sự lạ, ở đầu làng xuất hiện một con vật, trông giống con gà con, mà lại to hơn con gà con, cả làng ngạc nhiên không biết con gì, bèn kéo đến nhà mà hỏi. Mình lúc ấy đang ngồi trong wc, nghe thấy thế bèn trầm ngâm nghĩ ngợi, rồi truyền vọng ra rằng: "Trông giống con gà con, mà lại to hơn con gà con, ấy tất là con gà to". Dân làng lấy làm phục lắm, bèn bắt gà làm thịt, rồi tôn mình là bậc thánh nhân, cả làng từ ấy ăn gà không xuể.</p>
            <p>Còn tiếp...</p>
          </div>
        </div>

      </div>

      <!-- END main-content -->

      <div class="col-md-12 col-lg-4 sidebar">
        <div class="sidebar-box search-form-wrap">
          <form action="javascript:;" class="search-form">
            <div class="form-group">
              <span class="icon fa fa-search"></span>
              <input type="text"  class="form-control" id="search-on-page" placeholder="Nhập từ khóa và nhần enter!">
            </div>
          </form>
        </div>
        <!-- END sidebar-box -->
        <div class="sidebar-box">
          <h3 class="heading">Tác giả khác</h3>
          <div class="bio text-center">
            <img src="{{$random_author->avatar}}" alt="Image Placeholder" class="img-fluid">
            <div class="bio-body">
              <h2>{{$random_author->name}}</h2>
              <p>{{$random_author->quote}}</p>
              <p><a href="{{route('blog.about_author',$random_author->id)}}" class="btn btn-primary btn-sm rounded">xem thêm</a></p>
              <p class="social">
                <a href="#" class="p-2"><span class="fa fa-facebook"></span></a>
                <a href="#" class="p-2"><span class="fa fa-youtube-play"></span></a>
              </p>
            </div>
          </div>
        </div>
        <!-- END sidebar-box -->  
        <div class="sidebar-box">
          <h3 class="heading">Bài viết phổ biến</h3>
          <div class="post-entry-sidebar">
            <ul>
              @foreach($post_populars as $post)
              <li>
                <a href="/post/{{$post->slug}}">
                  <img src="{{ $post->thumbnail}}" alt="Image placeholder" class="mr-4">
                  <div class="text">
                    <h4>{{$post->title}}</h4>
                    <div class="post-meta">
                      <span class="mr-2">{{date_format($post->created_at, 'l jS F Y')}}</span>
                    </div>
                  </div>
                </a>
              </li>
              @endforeach
            </ul>
          </div>
        </div>
        <!-- END sidebar-box -->


      </div>
    </div>
  </div>
</section>
@endsection