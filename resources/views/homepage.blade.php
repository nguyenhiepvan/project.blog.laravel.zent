@extends('layouts.master')
@section('content')
<section class="site-section pt-5 pb-5">
  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <div class="owl-carousel owl-theme home-slider">
          @foreach($post_highlights as $post)
          <div>
            <a href="/post/{{$post->slug}}" class="a-block d-flex align-items-center height-lg" style="background-image: url('{{ $post->thumbnail}}') ">
              <div class="text half-to-full">
                <span class="category mb-5">{{ $post->category->name }}</span>
                <div class="post-meta">

                  <span class="author mr-2"><img src="{{ $post->user->avatar}}" alt="{{$post->user->name}}"> {{$post->user->name}}</span>&bullet;
                  <span class="mr-2">{{date_format($post->created_at, 'jS F Y')}}</span> &bullet;
                  <span class="ml-2"><span class="fa fa-comments"></span> {{$post->comments->count()}}</span>

                </div>
                <h3>{{ str_limit($post->description, $limit = 70, $end = ' ...xem tiếp') }}</h3>
                </div>
              </a>
            </div>
            @endforeach
          </div>

        </div>
      </div>
    </div>
  </section>
  <!-- END section -->

  <section class="site-section py-sm">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h2 class="mb-4">Bài đăng mới nhất</h2>
        </div>
      </div>
      <div class="row blog-entries">
        <div class="col-md-12 col-lg-8 main-content">
          <div class="row">
           @foreach($posts as $post)
           <div class="col-md-6">
            <a href="/post/{{$post->slug}}" class="blog-entry element-animate" data-animate-effect="fadeIn">
              <img src="{{ $post->thumbnail}}" width="350px" height="234px"  alt="Image placeholder">
              <div class="blog-content-body">
                <div class="post-meta">
                  <span class="author mr-2"><img src="{{$post->user->avatar}}" alt="{{$post->user->name}}"> {{$post->user->name}}</span>&bullet;
                  <span class="mr-2">{{date_format($post->created_at, 'jS F Y')}}</span> &bullet;
                  <span class="ml-2"><span class="fa fa-comments"></span> {{$post->comments->count()}}</span>
                </div>
                <h2>{{$post->title}}</h2>
              </div>
            </a>
          </div>
          @endforeach
        </div>

        <div class="row mt-5">
          <div class="col-md-12 text-center">
            <nav aria-label="Page navigation" class="text-center">
              {{ $posts->onEachSide(6)->links() }}
            </nav>
          </div>
        </div>

      </div>

      <!-- END main-content -->

      <div class="col-md-12 col-lg-4 sidebar">
        <div class="sidebar-box search-form-wrap">
          <form  action="javascript:;" class="search-form">
            <div class="form-group">
              <span class="icon fa fa-search"></span>
              <input type="text" class="form-control" id="search-on-page" placeholder="Nhập từ khóa và nhần enter!">
            </div>
          </form>
        </div>
        <!-- END sidebar-box -->
        <div class="sidebar-box">
          <h3 class="heading">Tác giả nổi bật</h3>
          <div class="bio text-center">
            <img src="{{ $author->avatar}}" alt="Image Placeholder" class="img-fluid">
            <div class="bio-body">
              <h2>{{$author->name}}</h2>
              <p>{{$author->quote}}</p>
              <p><a href="{{route('blog.about_author',$author->id)}}" class="btn btn-primary btn-sm rounded">Xem thêm</a></p>
              <p class="social">
                <a href="https://facebook.com/nguyenhiepvan.public" target="_blank" class="p-2"><span class="fa fa-facebook"></span></a>

                <a href="https://www.youtube.com/channel/UCluX5KAl97zL2GlKhol72Fw?view_as=subscriber" target="_blank" class="p-2"><span class="fa fa-youtube-play" class="p-2"></span></a>
              </p>
            </div>
          </div>
        </div>
        <!-- END sidebar-box -->  
        <div class="sidebar-box">
          <h3 class="heading">Bài viết phổ biến</h3>
          <div class="post-entry-sidebar">
            <ul>
              @foreach($post_populars as $post)
              <li>
                <a href="/post/{{$post->slug}}">
                  <img src="{{ $post->thumbnail}}" style="
                  width: 90px;
                  height: 60px;
                  " alt="Image placeholder" class="mr-4">
                  <div class="text">
                    <h4>{{$post->title}}</h4>
                    <div class="post-meta">
                      <span class="mr-2">{{date_format($post->created_at, 'jS F Y')}}</span>
                    </div>
                  </div>
                </a>
              </li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  @endsection