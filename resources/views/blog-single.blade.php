@extends('layouts.master')
@section('content')
<div class="row blog-entries element-animate">

  <div class="col-md-12 col-lg-8 main-content" style="
  text-align: justify;
  ">
  <img src="{{$post->thumbnail}}" alt="Image" class="img-fluid mb-5">
  <div class="post-meta">
    <span class="author mr-2"><img src="{{$post->user->avatar}}" alt="{{$post->user->name}}" class="mr-2"> {{$post->user->name}}</span>&bullet;
    <span class="mr-2">{{date_format($post->created_at, 'l jS F Y')}} </span> &bullet;
    <span class="ml-2"><span class="fa fa-comments"></span>{{$post->comments->count()}}</span>
  </div>
  <h1 class="mb-4">{{$post->title}}</h1>
  <a class="category mb-5" href="{{route('blog.getPostByCategory',$post->category->slug)}}">{{$post->category->name}}</a> 

  <div class="post-content-body">
    {!!html_entity_decode($post->content)!!}
  </div>


  <div class="pt-5">
    <p>Danh mục:  <a href="{{route('blog.getPostByCategory',$post->category->slug)}}">{{$post->category->name}}</a> <br>
      Thẻ: 
      @foreach($tags as $tag)
      <a href="{{route('blog.getPostByTag',$tag->slug)}}">#{{$tag->name}}</a> &nbsp
      @endforeach
    </p>
  </div>

  <div class="pt-5">
    <h3 class="mb-5">
      @if($comments->count()!=0)
      {{$comments->count()}} bình luận
      @else
      Chưa có bình luận
      @endif</h3>
      <ul class="comment-list" style="
      text-align: justify;
      ">
      @foreach($comments as $comment)
      @if($comment->parent_id == 0)
      @php
      $_comments = $comment->getParentComment();
      @endphp
      @if($_comments->isEmpty())
      <li class="comment">
        <div class="vcard">
          <img src="http://greenwings.co/wp-content/uploads/2018/09/blank-head-profile-pic-for-a-man.jpg" alt="Image placeholder">
        </div>
        <div class="comment-body">
          <h3>{{$comment->name}}
            @if($comment->email == $post->user->email)
            <sub style="color:blue">#Quản trị viên</sub>
            @endif
          </h3>
          <div class="meta">{{date_format($comment->created_at, 'l jS F Y')}}</div>
          <p>{{$comment->message}}</p>
          <p><a href="#leave_comment" data-parent="{{$comment->id}}" data-guest="{{$comment->id}}" class="reply rounded">Trả lời</a></p>
        </div>
      </li>
      @else
      <li class="comment">
        <div class="vcard">
          <img src="http://greenwings.co/wp-content/uploads/2018/09/blank-head-profile-pic-for-a-man.jpg" alt="Image placeholder">
        </div>
        <div class="comment-body">
          <h3>{{$comment->name}}
           @if($comment->email == $post->user->email)
           <sub style="color:blue">#Quản trị viên</sub>
           @endif
         </h3>
         <div class="meta">{{date_format($comment->created_at, 'l jS F Y')}}</div>
         <p>{{$comment->message}}</p>
         <p><a href="#leave_comment" data-parent="{{$comment->id}}" data-guest="{{$comment->id}}" class="reply rounded">Trả lời</a></p>
       </div>
     </li>
     <ul class="children">
      @foreach($_comments as $cmt)
      <li class="comment">
        <div class="vcard">
          <img src="http://greenwings.co/wp-content/uploads/2018/09/blank-head-profile-pic-for-a-man.jpg" alt="Image placeholder">
        </div>
        <div class="comment-body">
          <h3>{{$cmt->name}}
            @if($cmt->email == $post->user->email)
            <sub style="color:blue">#Quản trị viên</sub>
            @endif
          </h3>
          <div class="meta">{{date_format($cmt->created_at, 'l jS F Y')}}</div>
          <p>{{$cmt->message}}</p>
          <p><a href="#leave_comment" data-parent="{{$comment->id}}" data-guest="{{$cmt->id}}" class="reply rounded">Trả lời</a></p>
        </div>
      </li>
      @endforeach
    </ul>
    @endif  
    @endif
    @endforeach
  </ul>
  <!-- END comment-list -->

  <div class="comment-form-wrap pt-5" id="leave_comment" data-author="{{$post->user->id}}" data-post="{{$post->id}}">
    <h3 class="mb-5">Để lại bình luận</h3>
    <form action="javascript:;"  class="p-5 bg-light" id="comment-form">
      <div class="form-group">
        <label for="name">Họ và tên (<span class="error">*</span>)</label>
        <input type="text" class="form-control" id="name">
        <span class="name-error error" role="alert"></span>
      </div>
      <div class="form-group">
        <label for="email">Email (<span class="error">*</span>)</label>
        <input type="email" class="form-control" id="email">
        <span class="email-error error" role="alert"></span>
      </div>

      <div class="form-group">
        <label for="message">Bình luận (<span class="error">*</span>)</label>
        <textarea name="message" id="message" cols="30" rows="10" class="form-control"></textarea>
        <span class="message-error error" role="alert"></span>
      </div>
      <div class="form-group">
       <div class="g-recaptcha" id="feedback-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
       <span class="error-captcha error" role="alert"></span>
     </div>
     <div class="form-group">
       <div class="btn btn-primary ld-ext-right" id="comment-for-post">
         <span id="content_btn">Đăng bình luận</span>
         <div class="ld ld-ball ld-bounce"></div>
       </div>
     </div>

   </form>
 </div>
</div>

</div>

<!-- END main-content -->

<div class="col-md-12 col-lg-4 sidebar">
  <div class="sidebar-box search-form-wrap">
    <form action="javascript:;" class="search-form">
      <div class="form-group">
        <span class="icon fa fa-search"></span>
        <input type="text" class="form-control" id="search-on-page" placeholder="Type a keyword and hit enter">
      </div>
    </form>
  </div>
  <!-- END sidebar-box -->
  <div class="sidebar-box">
    <div class="bio text-center">
      <img src="{{$post->user->avatar}}" alt="Image Placeholder" class="img-fluid">
      <div class="bio-body">
        <h2>{{$post->user->name}}</h2>
        <p>{{$post->user->quote}}</p>
        <p><a href="{{route('blog.about_author',$post->user->id)}}" class="btn btn-primary btn-sm rounded">Xem thêm</a></p>
        <p class="social">
          <a href="#" class="p-2"><span class="fa fa-facebook"></span></a>
          <a href="#" class="p-2"><span class="fa fa-twitter"></span></a>
          <a href="#" class="p-2"><span class="fa fa-instagram"></span></a>
          <a href="#" class="p-2"><span class="fa fa-youtube-play"></span></a>
        </p>
      </div>
    </div>
  </div>
  <!-- END sidebar-box -->  
  <div class="sidebar-box">
    <h3 class="heading">Bài viết phổ biến</h3>
    <div class="post-entry-sidebar">
      <ul>
       @foreach($post_populars as $post)
       <li>
        <a href="/post/{{$post->slug}}">
          <img src="{{ $post->thumbnail}}" alt="Image placeholder" class="mr-4">
          <div class="text">
            <h4>{{$post->title}}</h4>
            <div class="post-meta">
              <span class="mr-2">{{date_format($post->created_at, 'l jS F Y')}}</span>
            </div>
          </div>
        </a>
      </li>
      @endforeach
    </ul>
  </div>
  <!-- END sidebar-box -->

  <div class="sidebar-box">
    <h3 class="heading">Danh mục</h3>
    <ul class="categories">
      @foreach($categories as $category)
      <li><a href="{{route('blog.getPostByCategory',$category->slug)}}">{{$category->name}} <span>({{$category->posts->count()}})</span></a></li>
      @endforeach
    </ul>
  </div>
  <!-- END sidebar-box -->

  <div class="sidebar-box">
    <h3 class="heading">Thẻ</h3>
    <ul class="tags">
      @foreach($tags as $tag)
      <li><a href="{{route('blog.getPostByTag',$tag->slug)}}">{{$tag->name}}</a></li>
      @endforeach
    </ul>
  </div>
</div>
<!-- END sidebar -->
</div>
@endsection

@section('script')

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="{{ asset('blog_assets/js/comment_custom.js')}}"></script>
@endsection

