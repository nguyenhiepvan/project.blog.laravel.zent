<!doctype html>
<html lang="en">
<head>
  <title>NH blog &mdash; laravel project</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300, 400,700|Inconsolata:400,700" rel="stylesheet">
  <link rel='icon' href="{{ asset('logo.png')}}" type='image/png'/ >

  <link rel="stylesheet" href="{{ asset('blog_assets/css/bootstrap.css')}}">
  <link rel="stylesheet" href="{{ asset('blog_assets/css/animate.css')}}">
  <link rel="stylesheet" href="{{ asset('blog_assets/css/owl.carousel.min.css')}}">

  <link rel="stylesheet" href="{{ asset('blog_assets/fonts/ionicons/css/ionicons.min.css')}}">
  <link rel="stylesheet" href="{{ asset('blog_assets/fonts/fontawesome/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{ asset('blog_assets/fonts/flaticon/font/flaticon.css')}}">

  <!-- Theme Style -->
  <link rel="stylesheet" href="{{ asset('blog_assets/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('blog_assets/css/loading.css')}}">
  <link rel="stylesheet" href="{{ asset('blog_assets/css/loading-btn.css')}}">
  <link rel="stylesheet" href="{{ asset('blog_assets/css/custom.css')}}">

  <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>


  <div class="wrap">

    <header role="banner">
      <div class="top-bar">
        <div class="container">
          <div class="row">
            <div class="col-9 social">
              <a href="#"><span class="fa fa-twitter"></span></a>
              <a href="https://facebook.com/nguyenhiepvan.public"><span class="fa fa-facebook"></span></a>
              <a href="#"><span class="fa fa-instagram"></span></a>
              <a href="https://www.youtube.com/channel/UCluX5KAl97zL2GlKhol72Fw?view_as=subscriber"><span class="fa fa-youtube-play"></span></a>
            </div>
            <div class="col-3 search-top">
              <!-- <a href="#"><span class="fa fa-search"></span></a> -->
              <form  class="search-top-form">
                <span class="icon fa fa-search"></span>
                <input type="text" id="search-on-top" placeholder="Bạn đang tìm gì?">
              </form>
            </div>
          </div>
        </div>
      </div>

      <div class="container logo-wrap">
        <div class="row pt-5">
          <div class="col-12 text-center">
            <a class="absolute-toggle d-block d-md-none" data-toggle="collapse" href="#navbarMenu" role="button" aria-expanded="false" aria-controls="navbarMenu"><span class="burger-lines"></span></a>
            <h1 class="site-logo"><a href="{{route('blog.homepage')}}"><img src="{{asset('blog_assets/images/logo1.png')}}" ></a></h1>
          </div>
        </div>
      </div>

      <nav class="navbar navbar-expand-md  navbar-light bg-light">
        <div class="container">


          <div class="collapse navbar-collapse" id="navbarMenu">
            <ul class="navbar-nav mx-auto">
              @foreach($categories as $category)
              @if($category->parent_id == null)
              @php
              $_categories = $category->getParentCategory();
              @endphp
              @if($_categories->isEmpty())
              <li class="nav-item id="{{$category->name}}">
                <a class="nav-link jaction" href="{{ route('blog.getPostByCategory',$category->slug)}}" >{{$category->name}}</a>
              </li>
              @else
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle jaction" href="{{ route('blog.getPostByCategory',$category->slug)}}" id="dropdown{{$category->parent_id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$category->name}}</a>
                <div class="dropdown-menu" aria-labelledby="dropdown{{$category->parent_id}}">
                  @foreach($_categories as $cate)
                  <a class="dropdown-item jaction" href="{{ route('blog.getPostByCategory',$cate->slug)}}" >{{$cate->name}}</a>
                  @endforeach
                </div> 
              </li>
              @endif  
              @endif
              @endforeach
              <li class="nav-item" id="about">
                <a class="nav-link jaction" href="{{route('blog.about')}}">Về chúng tôi</a>
              </li>
              <li class="nav-item" id="contact">
                <a class="nav-link jaction" href="{{route('blog.contact')}}" >Liên hệ</a>
              </li>
            </ul>

          </div>
        </div>
      </nav>
    </header>
    <!-- END header -->
    <div class="container " id="content">
      @yield('content')
    </div>


    <footer class="site-footer" style="
    padding-top: 35px;
    padding-bottom: 0px;
    ">
    <div class="container">
      <div class="row mb-5">
        <div class="col-md-4">
          <h3>Về chúng tôi</h3>
          <p class="mb-4">
            <img src="{{ asset('blog_assets/images/img_1.jpg')}}" alt="Image placeholder" class="img-fluid">
          </p>

          <p>Mình sinh ra tại việt nam.lớn lên trong một gia đình cần cù, giàu lòng yêu nước, sớm giác ngộ lý tưởng cách mạng.<a href="{{route('blog.about')}}">...đọc thêm</a></p>
        </div>
        <div class="col-md-6 ml-auto">
          <div class="row">
            <div class="col-md-7">
              <h3>Bài đăng mới nhất</h3>
              <div class="post-entry-sidebar">
                <ul>
                  @foreach($post_lastest as $post)
                  <li>
                    <a href="/post/{{$post->slug}}">
                      <img src="{{ $post->thumbnail}}" style="
                      width: 90px;
                      height: 60px;
                      " alt="Image placeholder" class="mr-4">
                      <div class="text">
                        <h4>{{$post->title}}</h4>
                        <div class="post-meta">
                          <span class="mr-2">{{date_format($post->created_at, 'jS F Y')}}</span> &bullet;
                          <span class="ml-2"><span class="fa fa-comments"></span>{{$post->comments->count()}}</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  @endforeach
                </ul>
              </div>
            </div>
            <div class="col-md-1"></div>

            <div class="col-md-4">

              <div class="mb-5">
                <h3>Đi tới</h3>
                <ul class="list-unstyled">
                  <li><a href="{{route('blog.about')}}">Về chúng tôi</a></li>
                  <li><a href="/admin/login">Đăng nhập</a></li>
                  <li><a href="/admin/register">Đăng ký làm tác giả</a></li>
                </ul>
              </div>

              <div class="mb-5">
                <h3>Mạng xã hội</h3>
                <ul class="list-unstyled footer-social">
                  <li><a href="facebook.com/nguyenhiepvan.public"><span class="fa fa-facebook"></span> Facebook</a></li>
                  <li><a href="https://www.youtube.com/channel/UCluX5KAl97zL2GlKhol72Fw?view_as=subscriber"><span class="fa fa-youtube-play"></span> Youtube</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 text-center">
          <p class="small">
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Bản quyền thuộc về &copy; Nguyễn Văn Hiệp| template được tạo <i class="fa fa-heart text-danger" aria-hidden="true"></i> bởi <a href="https://colorlib.com" target="_blank" >Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          </p>
        </div>
      </div>
    </div>
  </footer>
  <!-- END footer -->

</div>

<!-- loader -->
<div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.js"></script>
<script src="{{ asset('blog_assets/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{ asset('blog_assets/js/jquery-migrate-3.0.0.js')}}"></script>
<script src="{{ asset('blog_assets/js/popper.min.js')}}"></script>
<script src="{{ asset('blog_assets/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('blog_assets/js/owl.carousel.min.js')}}"></script>
<script src="{{ asset('blog_assets/js/jquery.waypoints.min.js')}}"></script>
<script src="{{ asset('blog_assets/js/jquery.stellar.min.js')}}"></script>
<script src="{{ asset('blog_assets/js/main.js')}}"></script>
<script src="{{ asset('blog_assets/js/post_custom.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script type="text/javascript">
  function resize(){
    if ($(window).width() < 768) { 
      $(".logo-wrap img").attr('src', 'http://project.blog.laravel.zent/blog_assets/images/logo_sub.png');
      $(".logo-wrap img").attr('width', '100%');
      $(".text").css('width', '');
    } else {
      $(".logo-wrap img").attr('src', 'http://project.blog.laravel.zent/blog_assets/images/logo1.png');
      $(".logo-wrap img").removeAttr('width');
       $(".text").css('width', '70%');
    }
  }
  resize();
  $(window).on('resize', resize);
</script>
<script type="text/javascript">
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
</script>
@yield('script')
</body>
</html>