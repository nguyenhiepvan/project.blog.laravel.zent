@extends('layouts.master')
@section('content')
<section class="site-section pt-5">
  <div class="container">
    <div class="row mb-4">
      <div class="col-md-6">
        <h2 class="mb-4">Thẻ: 
          {{$tag->name}}
        </h2>
      </div>
    </div>
    <div class="row blog-entries">
      <div class="col-md-12 col-lg-8 main-content">
        <div class="row mb-5 mt-5">

          <div class="col-md-12">
            @if($posts->isEmpty())
            <span>No post</span>
            @else
            @foreach($posts as $post)
            <div class="post-entry-horzontal">
              <a href="{{route('blog.blog-single',$post->slug)}}">
                <div class="image element-animate" data-animate-effect="fadeIn" style="background-image: url({{$post->thumbnail}});"></div>
                <span class="text">
                  <div class="post-meta">
                    <span class="author mr-2"><img src="{{ $post->user->avatar}}" alt="{{$post->user->name}}">{{$post->user->name}}</span>&bullet;
                    <span class="mr-2">{{date_format($post->created_at, 'l jS F Y')}}</span> &bullet;
                    <span class="mr-2">{{$post->category->name}}</span> &bullet;
                    <span class="ml-2"><span class="fa fa-comments"></span> {{$post->comments->count()}}</span>
                  </div>
                  <h2>{{$post->title}}</h2>
                </span>
              </a>
            </div>
            <!-- END post -->
            @endforeach
            @endif
          </div>
        </div>

        <div class="row mt-5">
          <div class="col-md-12 text-center">
            <nav aria-label="Page navigation" class="text-center">
             {{ $posts->onEachSide(6)->links() }}
           </nav>
         </div>
       </div>

       

     </div>

     <!-- END main-content -->

     <div class="col-md-12 col-lg-4 sidebar">
      <div class="sidebar-box search-form-wrap">
        <form action="javascript:;" class="search-form">
          <div class="form-group">
            <span class="icon fa fa-search"></span>
            <input type="text" class="form-control" id="search-on-page" placeholder="Nhập từ khóa và nhần enter!">
          </div>
        </form>
      </div>
      <!-- END sidebar-box -->
      <div class="sidebar-box">
        <h3 class="heading">Tác giả khác</h3>
        <div class="bio text-center">
          <img src="{{$random_author->avatar}}" alt="Image Placeholder" class="img-fluid">
          <div class="bio-body">
            <h2>{{$random_author->name}}</h2>
            <p>{{$random_author->quote}}</p>
            <p><a href="{{route('blog.about_author',$random_author->id)}}" class="btn btn-primary btn-sm rounded">xem thêm</a></p>
            <p class="social">
              <a href="#" class="p-2"><span class="fa fa-facebook"></span></a>
              <a href="#" class="p-2"><span class="fa fa-youtube-play"></span></a>
            </p>
          </div>
        </div>
      </div>
      <!-- END sidebar-box -->  
      <div class="sidebar-box">
        <h3 class="heading">Bài viết phổ biến</h3>
        <div class="post-entry-sidebar">
          <ul>
            @foreach($post_populars as $post)
            <li>
              <a href="/post/{{$post->slug}}">
                <img src="{{ $post->thumbnail}}" alt="Image placeholder" class="mr-4">
                <div class="text">
                  <h4>{{$post->title}}</h4>
                  <div class="post-meta">
                    <span class="mr-2">{{date_format($post->created_at, 'l jS F Y')}}</span>
                  </div>
                </div>
              </a>
            </li>
            @endforeach
          </ul>
        </div>
      </div>
      <!-- END sidebar-box -->
    </div>
  </div>
</section>
@endsection